


# Exercices

## Version 1

## Technologies utilisées

+ Laravel 7
+ Vue js

## Demarrer le projet

### Telecharger les dependances

    composer install
    
    
    npm install
    
### Executer et lancer le projet

- Lancer le serveur 

``` php artisan serve ```

- Compiler les dependances et demarrer vue

``` npm run dev ```

### Ou trouver le fichier excel

le fichier excel se situe dans le public et se nomme "doc.xlsx"
