/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vuetify from 'vuetify'
import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import Unicon from 'vue-unicons'
import { uniFire } from 'vue-unicons/src/icons'
import vCurrencySelect from 'vue-currency-select'
import 'vuetify/dist/vuetify.css'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'vue-currency-select/dist/vue-currency-select.css'
require('./bootstrap');
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import { routes } from './routes';

window.Vue = require('vue');

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(vCurrencySelect)
Unicon.add([uniFire])
Vue.use(Unicon)
Vue.use(VueSweetalert2)

const router = new VueRouter({
    mode: 'history',
    routes
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(),
    //render: h => h(App)
});
