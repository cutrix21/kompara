import App from './components/App'
import Inscription from "./components/Inscription";
import Home from "./components/Home";

export const routes = [
    {
        path: '/',
        component: App,
        children: [
            {
                path: '/',
                component: Home
            },
            {
                path: '/inscription',
                component: Inscription
            }
        ]
    }
]
