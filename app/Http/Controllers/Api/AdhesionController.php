<?php

namespace App\Http\Controllers\Api;

use App\Adhesion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AdhesionController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->all();

        $adhesion = DB::table('table_adherants')->insert($input);

        $adherants = DB::table('table_adherants')->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nom');
        $sheet->setCellValue('B1', 'Prenom');
        $sheet->setCellValue('C1', 'Email');
        $sheet->setCellValue('D1', 'Numero');

        foreach ($adherants as $i => $adherant) {
            $sheet->setCellValue('A'.($i+2), $adherant->nom);
            $sheet->setCellValue('B'.($i+2), $adherant->prenom);
            $sheet->setCellValue('C'.($i+2), $adherant->email);
            $sheet->setCellValue('D'.($i+2), $adherant->numero);
        }

        $writer = new Xlsx($spreadsheet);
        try {
            $writer->save('doc.xlsx');
            return response()->json(['message' => 'Adhesion effectuee avec success'], 201);
        } catch (Exception $e) {
        }

        //return response()->json(['message' => 'Success de votre adhesion'], 200);
    }
}
